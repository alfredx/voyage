FROM golang:latest as builder
WORKDIR /app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o voyage .

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/voyage .
CMD ["./voyage"]